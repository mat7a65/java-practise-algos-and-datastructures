package week35_2020.countnumberofteams;

public class Main {
	public static void main(String[] args) {
		
		int[] input = new int[] {1,2,3,4,5};
		
		int solution = Solution.numTeams(input);
		
		System.out.println(solution);
		
	}
}
