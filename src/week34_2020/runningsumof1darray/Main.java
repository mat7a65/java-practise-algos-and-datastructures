package week34_2020.runningsumof1darray;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] nums = new int[]{1, 2, 3, 4};

        System.out.println(Arrays.toString(Solution.runningSum(nums)));
    }
}
