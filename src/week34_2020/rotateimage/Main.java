package week34_2020.rotateimage;

public class Main {

    public static void main(String[] args) {
        int[][] image = {{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}};

        for (int i = 0; i < image.length; i++) {
            System.out.println();
            for (int j = 0; j < image.length; j++) {
                System.out.print(image[i][j] + ", ");
            }
        }

        Solution.rotate(image);
        System.out.println();

        for (int i = 0; i < image.length; i++) {
            System.out.println();
            for (int j = 0; j < image.length; j++) {
                System.out.print(image[i][j] + ", ");
            }
        }
    }
}
