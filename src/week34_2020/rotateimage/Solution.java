package week34_2020.rotateimage;

public class Solution {
    public static void rotate(int[][] matrix) {
        int n = matrix.length;

        for (int i = 0; i < n/2; i++) {
            for (int j = i; j < n - i - 1; j++) {

                int top = matrix[i][j];

                matrix[i][j] = matrix[n - 1 - j][i];

                matrix[n - 1 - j][i] = matrix[n - 1 - i][n - 1 - j];

                matrix[n - 1 - i][n - 1 - j] = matrix[j][n - i - 1];

                matrix[j][n - i - 1] = top;

            }
        }

    }
}
