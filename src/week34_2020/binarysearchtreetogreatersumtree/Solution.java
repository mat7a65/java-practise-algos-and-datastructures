package week34_2020.binarysearchtreetogreatersumtree;

public class Solution {
    static int sum = 0;

    public static TreeNode bstToGst(TreeNode root) {
        if(root == null) return root;
        bstToGst(root.right);
        sum += root.val;
        root.val = sum;
        bstToGst(root.left);
        return root;
    }
}
