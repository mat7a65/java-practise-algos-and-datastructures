package week34_2020.groupanagrams;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String[] strs = new String[]{"eat", "tea", "tan", "ate", "nat", "bat"};

        List<List<String>> solution = Solution.groupAnagrams(strs);

        System.out.println(solution);

        System.out.println(Solution2.groupAnagrams(strs));
    }
}
