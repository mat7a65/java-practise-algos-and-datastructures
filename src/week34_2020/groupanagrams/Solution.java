package week34_2020.groupanagrams;

import java.util.*;
public class Solution {
    public static List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> solution = new ArrayList<>();
        Set<Map<Character, Integer>> mapSet = new HashSet<>();

        for (String s : strs) {
            Map<Character, Integer> map = new HashMap<>();

            for (int i = 0; i < s.length(); i++) {
                int counter = 1;
                if (map.containsKey(s.charAt(i))) {
                    counter += map.get(s.charAt(i));
                    map.replace(s.charAt(i), counter);
                }
                map.put(s.charAt(i), counter);
            }
            mapSet.add(map);
        }

        mapSet.forEach(entry -> {
            List<String> stringList = new ArrayList<>();

            for (String s : strs) {
                Map<Character, Integer> map = new HashMap<>();
                for (int i = 0; i < s.length(); i++) {
                    int counter = 1;
                    if (map.containsKey(s.charAt(i))) {
                        counter += map.get(s.charAt(i));
                        map.replace(s.charAt(i), counter);
                    }
                    map.put(s.charAt(i), counter);
                }

                if(entry.equals(map)) {
                    stringList.add(s);
                }
            }
            solution.add(stringList);
        });

        return solution;
    }
}
