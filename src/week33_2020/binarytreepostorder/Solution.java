package week33_2020.binarytreepostorder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class Solution {
    public static List<Integer> postorderTraversal(TreeNode root) {

        List<Integer> integerList = new ArrayList<>();
        if (root == null) {return integerList;}

        Stack<TreeNode> treeNodeStack = new Stack<>();

        treeNodeStack.push(root);

        while (!treeNodeStack.isEmpty()) {
            TreeNode currentNode = treeNodeStack.pop();
            integerList.add(currentNode.val);

            if (currentNode.left != null) {
                treeNodeStack.push(currentNode.left);
            }
            if (currentNode.right != null) {
                treeNodeStack.push(currentNode.right);
            }
        }
        Collections.reverse(integerList);
        return integerList;
    }
}
