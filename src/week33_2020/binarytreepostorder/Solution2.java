package week33_2020.binarytreepostorder;

import java.util.ArrayList;
import java.util.List;

public class Solution2 {
    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> integerList = new ArrayList<>();
        recursive(root, integerList);
        return integerList;
    }

    public static void recursive(TreeNode root, List<Integer> integerList) {
        if(root != null) {

            recursive(root.left, integerList);
            recursive(root.right, integerList);
            integerList.add(root.val);
        }
    }
}
