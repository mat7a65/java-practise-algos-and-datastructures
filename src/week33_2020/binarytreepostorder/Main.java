package week33_2020.binarytreepostorder;

import java.util.List;

public class Main {
    public static void main(String[] args) {


        TreeNode neu = new TreeNode(1,
                new TreeNode(4, null, null),
                new TreeNode(5, new TreeNode(2, null, null),
                        new TreeNode(9, new TreeNode(8, null, null),
                                new TreeNode(7, null, null))));

        TreeNode neu2 = new TreeNode(1,
                new TreeNode(4, null, null),
                new TreeNode(5, new TreeNode(2, null, null),
                        new TreeNode(9, new TreeNode(8, null, null),
                                new TreeNode(7, null, null))));



        List<Integer> solutionList = Solution.postorderTraversal(neu);
        List<Integer> soutionList2 = Solution2.postorderTraversal(neu2);

        System.out.println(solutionList);
        System.out.println();
        System.out.println(soutionList2);

    }
}
