package week33_2020.queuereconstructionbyheigth;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        //      Input:
        //      [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]

        int[][] people = new int[][]{{7,0}, {4,4}, {7,1}, {5,0}, {6,1}, {5,2}};

        System.out.println(Arrays.deepToString(Solution.reconstructQueue(people)));
    }
}
