package week33_2020.queuereconstructionbyheigth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Solution {
    public static int[][] reconstructQueue(int[][] people) {

        Comparator<int[]> compareQueue = (a, b) -> {
            if (a[0] == b[0]) return a[1] - b[1];
            return b[0] - a[0];
        };

//        Comparator<int[]> compareQueue = (a1, b1) -> a1[0] == b1[0] ? a1[1] - b1[1] : b1[0] - a1[0];

        Arrays.sort(people, compareQueue);

        /*
        Sorting the Array by size/ height of the first index:
        [7, 0]
        [7, 1]
        [6, 1]
        [5, 0]
        [5, 2]
        [4, 4]
         */

        List<int[]> result = new ArrayList<>();


//        Put the element on the position with index k.

        for (int[] p : people) {
            result.add(p[1], p);
        }

        int n = people.length;

//        Create a new int-Array with n rows (number of entries) and two cells (-> h,k)
        int[][] orderedList = result.toArray(new int[n][2]);

        return orderedList;
    }
}
