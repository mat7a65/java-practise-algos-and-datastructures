package week33_2020.binarytreeinorder;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();

        TreeNode currentNode = root;
        TreeNode pre;

        while (currentNode != null) {
            if (currentNode.left == null) {
                result.add(currentNode.val);
                currentNode = currentNode.right;
            } else {
                pre = currentNode.left;
                while (pre.right != null && pre.right != currentNode) {
                    pre = pre.right;
                }

                if (pre.right == null) {
                    pre.right = currentNode;
                    currentNode = currentNode.left;
                } else {
                    pre.right = null;
                    result.add(currentNode.val);
                    currentNode = currentNode.right;
                }
            }
        }
        return result;
    }
}
