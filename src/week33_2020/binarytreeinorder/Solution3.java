package week33_2020.binarytreeinorder;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Solution3 {
    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> integerList = new ArrayList<>();
        Stack<TreeNode> treeNodeStack = new Stack<>();
        TreeNode currentNode = root;


        while (currentNode != null || !treeNodeStack.isEmpty()) {
            while (currentNode != null) {
                treeNodeStack.push(currentNode);
                currentNode = currentNode.left;
            }
            currentNode = treeNodeStack.pop();
            integerList.add(currentNode.val);
            currentNode = currentNode.right;
        }

        return integerList;
    }

}
