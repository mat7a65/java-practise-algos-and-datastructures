package week33_2020.binarytreeinorder;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        TreeNode neu = new TreeNode(1,
                new TreeNode(4, null, null),
                new TreeNode(5, new TreeNode(2, null, null),
                        new TreeNode(9, new TreeNode(8, null, null),
                                new TreeNode(7, null, null))));



        List<Integer> listSolution = Solution.inorderTraversal(neu);
        List<Integer> listSolution2 = Solution2.inorderTraversal(neu);
        List<Integer> listSolution3 = Solution3.inorderTraversal(neu);

        System.out.println(listSolution);
        System.out.println();
        System.out.println(listSolution2);
        System.out.println();
        System.out.println(listSolution3);

    }
}
