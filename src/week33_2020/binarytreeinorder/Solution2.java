package week33_2020.binarytreeinorder;

import java.util.ArrayList;
import java.util.List;

public class Solution2 {
    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> integerList = new ArrayList<>();
        recursive(root, integerList);
        return integerList;
    }

    public static void recursive(TreeNode root, List<Integer> integerList) {
        if(root != null) {
            recursive(root.left, integerList);
            integerList.add(root.val);
            recursive(root.right, integerList);
        }
    }
}
