package week33_2020.binarytreepreorder;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        TreeNode neu = new TreeNode(1,
                new TreeNode(4, null, null),
                new TreeNode(5, new TreeNode(2, null, null),
                        new TreeNode(9, new TreeNode(8, null, null),
                                new TreeNode(7, null, null))));


        TreeNode neu2 = new TreeNode(1,
                new TreeNode(4, null, null),
                new TreeNode(5, new TreeNode(2, null, null),
                        new TreeNode(9, new TreeNode(8, null, null),
                                new TreeNode(7, null, null))));

        TreeNode neu3 = new TreeNode(1,
                new TreeNode(4, null, null),
                new TreeNode(5, new TreeNode(2, null, null),
                        new TreeNode(9, new TreeNode(8, null, null),
                                new TreeNode(7, null, null))));

        List<Integer> solutionList = Solution.preorderTraversal(neu);
        List<Integer> solutionList2 = Solution2.preorderTraversal(neu2);
        List<Integer> solutionList3 = Solution3.preorderTraversal(neu3);

        System.out.println(solutionList);
        System.out.println();
        System.out.println(solutionList2);
        System.out.println();
        System.out.println(solutionList3);
    }
}
