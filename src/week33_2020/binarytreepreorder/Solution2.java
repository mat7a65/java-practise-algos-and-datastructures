package week33_2020.binarytreepreorder;

import java.util.ArrayList;
import java.util.List;

public class Solution2 {
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> integerList = new ArrayList<>();
        recursive(root, integerList);
        return integerList;
    }

    public static void recursive(TreeNode root, List<Integer> integerList) {
        if (root != null) {
            integerList.add(root.val);
            recursive(root.left, integerList);
            recursive(root.right, integerList);
        }
    }
}
