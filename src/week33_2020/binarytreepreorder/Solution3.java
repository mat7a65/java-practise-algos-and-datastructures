package week33_2020.binarytreepreorder;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Solution3 {
    public static List<Integer> preorderTraversal(TreeNode root) {

        List<Integer> integerList = new ArrayList<>();
        if (root == null) return integerList;

        Stack<TreeNode> treeNodeStack = new Stack<>();

        treeNodeStack.push(root);

        while (!treeNodeStack.isEmpty()) {
            TreeNode currentNode = treeNodeStack.pop();
            integerList.add(currentNode.val);

            if (currentNode.right != null) {
                treeNodeStack.push(currentNode.right);
            }
            if (currentNode.left != null) {
                treeNodeStack.push(currentNode.left);
            }
        }
        return integerList;
    }
}
