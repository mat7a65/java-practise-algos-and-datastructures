package week33_2020.binarytreepreorder;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<Integer> preorderTraversal(TreeNode node) {

        List<Integer> integerList = new ArrayList<>();

        while (node != null) {

            if (node.left == null) {
                integerList.add(node.val);
                node = node.right;
            } else {

                TreeNode currentNode = node.left;

                while (currentNode.right != null && currentNode.right != node) {
                    currentNode = currentNode.right;
                }

                if (currentNode.right == node) {
                    node = node.right;
                } else {
                    integerList.add(node.val);
                    currentNode.right = node;
                    node = node.left;
                }
            }
        }
        return integerList;
    }
}
