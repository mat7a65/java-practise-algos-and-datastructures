package week33_2020.dailytemperatures;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] T = new int[]{73, 74, 75, 71, 69, 72, 76, 73};

        int[] T1 = new int[]{55,38,53,81,61,93,97,32,43,78};

        System.out.println(Arrays.toString(Solution.dailyTemperatures(T)));
        System.out.println(Arrays.toString(Solution.dailyTemperatures(T1)));
    }
}
