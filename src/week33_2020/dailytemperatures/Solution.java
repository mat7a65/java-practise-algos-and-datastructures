package week33_2020.dailytemperatures;

public class Solution {
    public static int[] dailyTemperatures(int[] T) {

        //  T = [73, 74, 75, 71, 69, 72, 76, 73]
        //  W = [1, 1, 4, 2, 1, 1, 0, 0]

        int length = T.length;
        int[] wait = new int[length];

        for (int i = 0; i < T.length-1; i++) {
            int j = i + 1;

            while (T[j] <= T[i]) {
                j++;

                if (j >= T.length) {
                    j = i;
                    break;
                }
            }
            wait[i] = (j - i);
        }
        return wait;
    }
}
