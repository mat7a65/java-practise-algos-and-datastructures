package week33_2020.powerofthree;

public class Main {
    public static void main(String[] args) {
        System.out.println("Solution 1: ");
        System.out.println(Solution.isPowerOfThree(1));
        System.out.println(Solution.isPowerOfThree(27));
        System.out.println(Solution.isPowerOfThree(0));
        System.out.println(Solution.isPowerOfThree(9));
        System.out.println(Solution.isPowerOfThree(2147483647));
    }
}
