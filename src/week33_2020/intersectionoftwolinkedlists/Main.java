package week33_2020.intersectionoftwolinkedlists;

public class Main {
    public static void main(String[] args) {
        ListNode nodeA1 = new ListNode(4);
        ListNode nodeA2 = new ListNode(1);

        ListNode nodeB1 = new ListNode(5);
        ListNode nodeB2 = new ListNode(6);
        ListNode nodeB3 = new ListNode(1);

        ListNode nodeC1 = new ListNode(8);
        ListNode nodeC2 = new ListNode(4);
        ListNode nodeC3 = new ListNode(5);

        nodeA1.next = nodeA2;
        nodeA2.next = nodeC1;

        nodeB1.next = nodeB2;
        nodeB2.next = nodeB3;
        nodeB3.next = nodeC1;

        nodeC1.next = nodeC2;
        nodeC2.next = nodeC3;
        nodeC3.next = null;


        System.out.println("Solution 1:");
        ListNode intersectionNode = Solution.getIntersectionNode(nodeA1, nodeB1);
        System.out.println(intersectionNode.val);
        System.out.println(intersectionNode);

        System.out.println();

        System.out.println("Solution 2:");
        ListNode intersectionNode2 = Solution2.getIntersectionNode(nodeA1, nodeB1);
        System.out.println(intersectionNode2.val);
        System.out.println(intersectionNode2);

        System.out.println();

        System.out.println("Solution 3:");
        ListNode intersectionNode3 = Solution3.getIntersectionNode(nodeA1, nodeB1);
        System.out.println(intersectionNode3.val);
        System.out.println(intersectionNode3);

    }
}
