package week33_2020.intersectionoftwolinkedlists;

import java.util.ArrayList;
import java.util.List;

public class Solution3 {
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {

        ListNode currentNodeA = headA;
        List<ListNode> nodeListB = new ArrayList<>();

        while (headB != null) {
            nodeListB.add(headB);
            headB = headB.next;
        }

        while (currentNodeA != null) {
            for (ListNode currentNode : nodeListB) {
                if (currentNodeA == currentNode) {
                    return currentNode;
                }
            }
            currentNodeA = currentNodeA.next;
        }
        return null;
    }
}
