package week33_2020.intersectionoftwolinkedlists;

public class Solution2 {
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {

        ListNode currentNodeA = headA;
        ListNode currentNodeB;

        while (currentNodeA != null) {
            for (currentNodeB = headB; currentNodeB != null; currentNodeB = currentNodeB.next) {
                if (currentNodeA == currentNodeB) {
                    return currentNodeA;
                }
            }
            currentNodeA = currentNodeA.next;
        }
        return null;
    }
}
