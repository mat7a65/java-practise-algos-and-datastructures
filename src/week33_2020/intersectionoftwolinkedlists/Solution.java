package week33_2020.intersectionoftwolinkedlists;

import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Set<ListNode> nodesColletion = new HashSet<>();
        ListNode currentNodeA = headA;

        while (currentNodeA != null) {
            nodesColletion.add(currentNodeA);
            currentNodeA = currentNodeA.next;
        }
        if (nodesColletion.isEmpty()) {
            return null;
        }

        ListNode currentNodeB = headB;
        while (currentNodeB != null) {
            if (nodesColletion.contains(currentNodeB)) {
                return currentNodeB;
            }
            currentNodeB = currentNodeB.next;
        }
        return null;
    }
}
