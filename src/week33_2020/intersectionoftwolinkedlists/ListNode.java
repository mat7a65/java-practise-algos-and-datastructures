package week33_2020.intersectionoftwolinkedlists;

public class ListNode {
    int val;
    ListNode next;

    public ListNode(int x) {
        this.val = x;
        this.next = null;
    }
}
