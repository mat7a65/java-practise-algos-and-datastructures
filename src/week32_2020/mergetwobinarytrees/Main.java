package week32_2020.mergetwobinarytrees;

public class Main {
    public static void main(String[] args) {

        TreeNode t1 = new TreeNode(1,
                new TreeNode(3, new TreeNode(5), null),
                new TreeNode(2, null, null));

        TreeNode t2 = new TreeNode(2,
                new TreeNode(1, null,
                        new TreeNode(4, null, null)),
                new TreeNode(3, null,
                        new TreeNode(7, null, null)));

        TreeNode soution = Solution.mergeTrees(t1, t2);

        TreeNode solution2 = Solution2.mergeTrees(t1, t2);
    }
}
