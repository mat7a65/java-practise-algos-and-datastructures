package week32_2020.maxdepthofbinarytree;

public class Main {

    public static void main(String[] args) {
        TreeNode tree = new TreeNode(3,
                new TreeNode(9, null, null),
                new TreeNode(20,
                        new TreeNode(15, null, null),
                        new TreeNode(7, null, null)));

        int height = Solution.maxDepth(tree);
        System.out.println("Maximum height ist: " +height);
    }
}
