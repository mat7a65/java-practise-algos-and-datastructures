package week32_2020.maxdepthofbinarytree;

public class Solution {
    public static int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        } else return (1 + Math.max(
                maxDepth(root.left),
                maxDepth(root.right)
        ));
    }
}
