package week32_2020.majorityelement;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static int majorityElement(int[] nums) {
        final int[] result = new int[1];
        Map<Integer, Integer> integerMap = new HashMap<>();
        for(int i : nums) {
            integerMap.put(i, integerMap.getOrDefault(i, 0) + 1);
        }

        integerMap.forEach( (key, value) -> {
            if(value > nums.length/2) {
                result[0] = key;
            }
        });
        return result[0];
    }
}
