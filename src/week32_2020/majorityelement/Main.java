package week32_2020.majorityelement;

public class Main {
    public static void main(String[] args) {
        int[] array1 = {3, 2, 3};
        int[] array2 = {2, 2, 1, 1, 1, 2, 2};

        System.out.println(Solution.majorityElement(array1));
        System.out.println(Solution.majorityElement(array2));

        System.out.println(Solution2.majorityElement(array1));
        System.out.println(Solution2.majorityElement(array2));

    }
}
