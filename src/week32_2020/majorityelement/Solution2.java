package week32_2020.majorityelement;

public class Solution2 {
    public static int majorityElement(int[] nums) {
        int maxCount = nums.length / 2;

        for (int num : nums) {
            int counter = 0;
            for (int element : nums) {
                if (element == num) {
                    counter++;
                }
            }

            if (counter > maxCount) {
                return num;
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}
