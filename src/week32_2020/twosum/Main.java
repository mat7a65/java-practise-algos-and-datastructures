package week32_2020.twosum;

public class Main {
    public static void main(String[] args) {
        int[] nums = {2, 7, 11, 15};
        int target = 9;
        int solution [];

        solution = Solution.twoSum(nums, target);
        System.out.println(solution[0] + " " + solution[1]);
    }
}
