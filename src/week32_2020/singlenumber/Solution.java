package week32_2020.singlenumber;

import java.util.Arrays;

public class Solution {
    public static int singleNumber(int[] nums) {

        for (int i = 0; i <= nums.length - 1; i++) {
            int tempValue = nums[i];
            boolean hasMatcher = false;

            for (int j = 0; j <= nums.length - 1; j++) {
                if (i != j) {
                    if (nums[i] == nums[j]) {
                        hasMatcher = true;
                    }
                }
            }
            if (!hasMatcher) {
                return tempValue;
            }
        }
        throw new IllegalArgumentException("All numbers are matching");
    }
}
