package week32_2020.singlenumber;

import java.util.*;

public class Solution2 {

    public static int singleNumber(int[] nums) {

        final int[] result = new int[1];

        Map<Integer, Integer> integerHashMap = new HashMap<>();

        for (int i : nums) {
            integerHashMap.put(i, integerHashMap.getOrDefault(i, 0) + 1);
        }

        integerHashMap.forEach( (key, value) -> {
            if(value == 1) {
                result[0] = key;
            }
        });
        return result[0];
    }
}
