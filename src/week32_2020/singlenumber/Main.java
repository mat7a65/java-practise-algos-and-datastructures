package week32_2020.singlenumber;

public class Main {
    public static void main(String[] args) {

        int[] array1 = {2, 2, 1};
        int[] array2 = {1, 1, 2, 2, 5};

        System.out.println(Solution.singleNumber(array1));
        System.out.println(Solution.singleNumber(array2));

        Solution2.singleNumber(array2);
    }
}
