package week32_2020.symmetrictree;

public class Main {
    public static void main(String[] args) {
        TreeNode tree = new TreeNode(1,
                new TreeNode(2,
                        new TreeNode(3, null, null),
                        new TreeNode(4, null, null)),
                new TreeNode(2,
                        new TreeNode(4, null, null),
                        new TreeNode(3, null, null))
        );

        boolean solution = Solution.isSymmetric(tree);

        System.out.println(solution);
    }
}
